import streamlit as st
from PIL import Image
from streamlit_cropper import st_cropper

from features.build_features import get_sites
from features.process_data import get_data
from setup.database import db


def run_crop():
    sites = get_sites()
    site_select = st.sidebar.selectbox("Select Site", sites.keys())
    site_id = sites[site_select]["site_id"]
    _, img = get_data(site_id, "Clear")

    with st.beta_expander("Region Cropper", True):
        c1_1, c1_2 = st.beta_columns((3, 1))
        if site_select:
            with c1_1:
                img = Image.fromarray(img)
                cropped_img = st_cropper(img, aspect_ratio=None, return_type="box")
                pil_ratio = img.size[0] / 700
                right = cropped_img["left"] + cropped_img["width"]
                bottom = cropped_img["top"] + cropped_img["height"]

                left_scaled = round(cropped_img["left"] * pil_ratio)
                top_scaled = round(cropped_img["top"] * pil_ratio)
                right_scaled = round(right * pil_ratio)
                bottom_scaled = round(bottom * pil_ratio)
            with c1_2:
                site_id = sites[site_select]["site_id"]
                x_coords = left_scaled, right_scaled
                y_coords = top_scaled, bottom_scaled

                st.markdown(
                    f"""
                Site Name: `{site_select}` \n
                Site ID: `{site_id}`
                """
                )
                st.subheader("")
                st.success(
                    f"""
                Active Crop WIndow \n
                X-coord: `{x_coords}` \n
                Y-coord: `{y_coords}`
                """
                )
                button_store = st.button("Store coordinates")
                if button_store:
                    query = {"site_id": site_id}
                    update = {"$set": {"x_coords": x_coords, "y_coords": y_coords}}
                    db["kk_cv_images"].update(query, update, multi=True)

    with st.beta_expander("Stored Coordinates", True):
        query = {"site_state": "Clear"}
        project = {"_id": 0, "site_name": 1, "site_id": 1, "x_coords": 1, "y_coords": 1}
        st.dataframe(db["kk_cv_images"].find(query, project))
