import pandas as pd
import streamlit as st
from st_aggrid import GridOptionsBuilder, AgGrid, GridUpdateMode, JsCode

from setup.database import db
from features.build_features import calculate_ssim
from visualization.plot import plot_marked_map


def run_dashboard():
    c1_1, c1_2 = st.beta_columns((2, 3))
    with c1_1:
        st.subheader("Alarms")
        query = {"site_state": "Blocked"}
        project = {"_id": 0, "site_id": 1, "site_name": 1, "chan-vese": 1}
        df_scores = pd.DataFrame(db["kk_cv_images"].find(query, project))
        df_scores = df_scores[["site_id", "site_name", "chan-vese"]]
        df_scores.columns = ["ID", "Description", "Rating"]

        gb = GridOptionsBuilder.from_dataframe(df_scores)
        gb.configure_selection("single")

        cellstyle_jscode = JsCode("""
        function(params) {
            if (params.value < 0.5) {
                return {
                    'backgroundColor': '#ffcccb'
                }
                }
            else if (params.value > 0.5 && params.value < 0.8) {
                return {
                    'backgroundColor': '#fed8b1'
                }
                } 
            else {
                return {
                    'backgroundColor': '#e3fbe3'
                }
            }
        };
        """)
        gb.configure_column("Rating", cellStyle=cellstyle_jscode)

        grid_response = AgGrid(
            df_scores,
            gridOptions=gb.build(),
            height=665,
            width=180,
            update_mode=GridUpdateMode.SELECTION_CHANGED,
            fit_columns_on_grid_load=True,
            allow_unsafe_jscode=True,
        )

        selected = grid_response["selected_rows"]
        if len(selected) == 0:
            selected = df_scores.iloc[0].to_dict()
        else:
            selected = grid_response["selected_rows"][0]

    with c1_2:
        st.subheader("")
        highlight = st.checkbox("Highlight", True)
        _, _, fig_res = calculate_ssim(
            selected["Description"], "Clear", "Blocked", "Chan-Vese", highlight
        )
        st.pyplot(fig_res)
        fig_map = plot_marked_map(selected["ID"])
        st.plotly_chart(fig_map, use_container_width=True)