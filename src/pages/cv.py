import streamlit as st

from features.build_features import calculate_ssim, get_sites
from features.process_data import get_data


def run_cv():
    sites = get_sites()

    with st.sidebar:
        list_site = ["Feltham", "Kinson", "Vernslade", "Westway"]
        select_site = st.selectbox("Select Site", list_site)

        list_state = ["Clear", "Blocked"]
        select_im1 = st.selectbox("Select Image 1", list_state)
        select_im2 = st.selectbox("Select Image 2", list_state, index=1)

        list_method = ["Chan-Vese", "Canny", "Original"]
        select_method = st.selectbox("Select Method", list_method)

    c1_1, c1_2, c1_3 = st.beta_columns(3)

    site_id = sites[select_site]["site_id"]
    select_im1_val = sites[select_site][select_im1]
    select_im2_val = sites[select_site][select_im2]

    score1, fig1, fig_res = calculate_ssim(
        select_site,
        select_im1,
        select_im2,
        select_method,
    )

    _, image_im1 = get_data(site_id, select_im1)
    _, image_im2 = get_data(site_id, select_im2)

    with st.beta_expander("Image Differences", expanded=True):
        st.pyplot(fig1)

    with c1_1:
        st.success(
            f"""
            Similarity Score: `{round(score1, 2)}`
            """
        )
        st.markdown(
            f"""
        Site Name: `{select_site}` \n
        Site ID: `{site_id}`
        """
        )
