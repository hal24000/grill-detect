import streamlit as st

from pages.crop import run_crop
from pages.cv import run_cv
from pages.dashboard import run_dashboard
from setup.database import db
from setup.layout import setup_page


setup_page(name="Grill Computer Vision")
list_page = ["Dashboard", "Computer Vision", "Region Cropper"]
select_page = st.sidebar.selectbox("Select Page", list_page)

if select_page == list_page[0]:
    run_dashboard()

if select_page == list_page[1]:
    run_cv()

if select_page == list_page[2]:
    run_crop()

with st.sidebar:
    st.header("")
    st.info(
        """
    ### :information_source: Info
    - Computer Vision for automated blockage detection.
    - Calculating difference in images to detect anomalies.
    """
    )
