import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

from setup.database import db


def plot_marked_map(select_site):
    query = {"site_state": "Blocked"}
    project = {
        "_id": 0,
        "site_id": 1,
        "site_name": 1,
        "lat": 1,
        "lon": 1,
        "chan-vese": 1,
    }
    df = pd.DataFrame(db["kk_cv_images"].find(query, project))

    risk_list = []
    for i, row in df.iterrows():
        if row["chan-vese"] < 0.5:
            risk_list.append("High")
        elif (row["chan-vese"] > 0.5) & (row["chan-vese"] < 0.8):
            risk_list.append("Med")
        else:
            risk_list.append("Low")
    df["Risk"] = risk_list

    symbols = []

    for i, row in df.iterrows():
        if row["site_id"] == select_site:
            symbols.append("triangle")
        else:
            symbols.append("circle")

    mapbox_access_token = open("setup/.mapbox_token").read()
    fig = go.Figure(
        go.Scattermapbox(
            name="",
            mode="markers",
            lat=df["lat"],
            lon=df["lon"],
            customdata=df[["site_name", "site_id", "Risk", "chan-vese"]],
            hovertemplate="""
        <b>Name</b>: %{customdata[0]} <br>
        <b>ID</b>: %{customdata[1]} <br>
        <b>Risk</b>: %{customdata[2]} <br>
        <b>Rating</b>: %{customdata[3]}
        """,
            marker=dict(
                size=10,
                color=df["Risk"]
                .map({"High": "red", "Med": "orange", "Low": "lightgreen"})
                .tolist(),
            ),
        )
    )
    fig.update_layout(
        legend_title_text="",
        legend=dict(
            x=0,
            y=0.10,
            traceorder="reversed",
            orientation="h",
            bgcolor="rgba(0,0,0,0)",
            font=dict(size=13),
        ),
        height=350,
        margin={"r": 10, "t": 10, "l": 10, "b": 10},
        mapbox_style="dark",
        hovermode="closest",
        mapbox=dict(
            accesstoken=mapbox_access_token,
            center=dict(
                lat=df[df["site_id"] == select_site]["lat"].values[0],
                lon=df[df["site_id"] == select_site]["lon"].values[0],
            ),
            zoom=8,
        ),
    )
    return fig
