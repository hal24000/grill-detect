import base64
import cv2


import pandas as pd
import numpy as np
import streamlit as st
import matplotlib.pyplot as plt

from PIL import Image, ImageEnhance
from io import BytesIO

from skimage import exposure, img_as_float, filters, feature, metrics
from skimage.segmentation import chan_vese
from skimage.util import img_as_uint, img_as_ubyte, compare_images

# from st_aggrid import GridOptionsBuilder, AgGrid, GridUpdateMode
from os import listdir
from os.path import isfile, join
from matplotlib.gridspec import GridSpec


# from setup.database import db
from setup.layout import setup_page


#### Functions used - to be moved to seperate files


def get_exposure_histogram(exposure_correction, imgsk, bins=250):
    """Gets the histogram from ..."""
    fig, ax = plt.subplots()
    ax.hist(exposure_correction.ravel(), bins=bins, histtype="step", color="black")
    img_cdf, bins = exposure.cumulative_distribution(imgsk, bins)
    ax_cdf = ax.twinx()
    ax_cdf.plot(bins, img_cdf, "r")
    return fig


def get_image_download_link(img, filename, text):
    """Download image to local downloads folder"""
    buffered = BytesIO()
    img.save(buffered, format="png")
    img_str = base64.b64encode(buffered.getvalue()).decode()
    print(filename)
    filename = f"..&sol;Documents&sol;{filename}"
    print(filename)
    href = f'<a href="data:file/txt;base64,{img_str}" download="{filename}" style = "display:block; text-align: center; color: black;" >{text}</a>'
    return href


def get_image_edges(image):
    """Gets the edges of an image using various filters etc"""
    dict_edges = {}
    dict_edges["edge_roberts"] = filters.roberts(image)
    dict_edges["edge_sobel"] = filters.sobel(image)
    dict_edges["edge_prewitt"] = filters.prewitt(image)
    dict_edges["edge_farid"] = filters.farid(image)
    dict_edges["edge_laplace"] = filters.laplace(image)
    dict_edges["edge_scharr"] = filters.scharr(image)
    dict_edges["edge_canny"] = feature.canny(image, sigma=1)
    dict_edges["edge_canny3"] = feature.canny(image, sigma=3)
    chan = chan_vese(
        image,
        mu=0.5,
        lambda1=1,
        lambda2=1,
        tol=1e-3,
        max_iter=200,
        dt=0.5,
        init_level_set="checkerboard",
        extended_output=True,
    )
    dict_edges["chan_vese_seg"] = chan[0]
    dict_edges["chan_vese_output"] = chan[1]

    return dict_edges


def plot_image_edges(image):
    """Plots the edges of the images using dictionary of edges"""

    dict_edges = get_image_edges(image=image)

    fig, ax = plt.subplots(ncols=3, nrows=3, sharex=True, sharey=True, figsize=(8, 4))

    # ax[0,0].imshow(image, cmap=plt.cm.gray)
    # ax[0,0].set_title('Original Image')

    ax[0, 0].imshow(dict_edges["edge_scharr"], cmap=plt.cm.gray)
    ax[0, 0].set_title("Scharr")

    ax[0, 1].imshow(dict_edges["edge_roberts"], cmap=plt.cm.gray)
    ax[0, 1].set_title("Roberts")

    ax[0, 2].imshow(dict_edges["edge_farid"], cmap=plt.cm.gray)
    ax[0, 2].set_title("Farid")

    ax[1, 0].imshow(dict_edges["edge_sobel"], cmap=plt.cm.gray)
    ax[1, 0].set_title("Sobel")

    ax[1, 1].imshow(dict_edges["edge_prewitt"], cmap=plt.cm.gray)
    ax[1, 1].set_title("Prewitt")

    ax[1, 2].imshow(dict_edges["edge_laplace"], cmap=plt.cm.gray)
    ax[1, 2].set_title("Laplace")

    ax[2, 0].imshow(dict_edges["edge_canny"], cmap=plt.cm.gray)
    ax[2, 0].set_title("Canny")

    ax[2, 1].imshow(dict_edges["chan_vese_seg"], cmap=plt.cm.gray)
    ax[2, 1].set_title("Chan Vese Segmentation")

    ax[2, 2].imshow(dict_edges["chan_vese_output"], cmap=plt.cm.gray)
    ax[2, 2].set_title("Chan Vese")
    plt.tight_layout()
    return fig


def get_edge_image_dicts(image_clear, image_blocked):
    """Gets edge dict for clear and unblocked images - change to be just 1 image and call for each"""

    dict_edges_blocked = get_image_edges(image=image_blocked)
    dict_edges_clear = get_image_edges(image=image_clear)

    return dict_edges_blocked, dict_edges_clear


def compare_images_view(dict_blocked, dict_unblocked, filter_="edge_scharr"):
    """compare two images from image edge dict using filter_ specified"""
    img1 = dict_unblocked[filter_]
    img2 = dict_blocked[filter_]
    diff_rotated = compare_images(img1, img2, method="diff")
    fig = plt.figure(figsize=(8, 9))

    gs = GridSpec(1, 3)
    ax0 = fig.add_subplot(gs[0, 0])
    ax1 = fig.add_subplot(gs[0, 1])
    ax2 = fig.add_subplot(gs[0, 2])

    ax0.imshow(img1, cmap="gray")
    ax0.set_title("blocked")
    ax1.imshow(img2, cmap="gray")
    ax1.set_title("clear")
    ax2.imshow(diff_rotated, cmap="gray")
    ax2.set_title("Diff comparison")
    fig.suptitle(
        f"""Blockage risk of:  {round(abs(1 - metrics.structural_similarity(img1, img2))*100, 2) } %, from Similarity score of {round(metrics.structural_similarity(img1, img2), 2)}, using filter: {filter_}""",
        fontsize=20,
        y=0.65,
    )
    for a in (ax0, ax1, ax2):
        a.axis("off")
    plt.tight_layout()
    return fig


############# APP ##########################

setup_page(name="Inlet/Outlet Blockage Detector")


st.title("Inlet/outlet Blockage Model Builder")

uploaded_file = st.file_uploader(
    "Choose an image...",
    type="png",
)

if uploaded_file is not None:
    image = Image.open(uploaded_file)  # format for brightness/contrast/colur/sharpness
    imgsk = img_as_float(image)  # format for gamma/log transformations
    cv_img = np.array(image)[:, :, 0]  # format for edges

    st.image(image, caption="Uploaded Image.", use_column_width=True)

    st.title("Preprocessing")

    with st.beta_expander("Preprocessing - Colour, Brighness, Contast, Sharpness"):  #
        c1_1, c1_2, c1_3, c1_4 = st.beta_columns((1, 1, 1, 1))
        with c1_1:
            enh_col = ImageEnhance.Color(image)
            colour = st.slider(
                "", min_value=-100.0, max_value=100.0, value=1.0, step=0.5, key="color1"
            )
            image_colored = enh_col.enhance(colour)
            st.image(image_colored, caption=f" Colour: {colour}", use_column_width=True)
            st.markdown(
                get_image_download_link(
                    img=image_colored, filename="test.png", text="save picture"
                ),
                unsafe_allow_html=True,
            )

        with c1_2:
            enh_bri = ImageEnhance.Brightness(image)
            brightness = st.slider(
                "",
                min_value=-100.0,
                max_value=100.0,
                value=1.0,
                step=0.5,
                key="brightness1",
            )
            image_brightened = enh_bri.enhance(brightness)
            st.image(
                image_brightened,
                caption=f" Brighness: {brightness}",
                use_column_width=True,
            )
            st.markdown(
                get_image_download_link(
                    img=image_brightened, filename="test.png", text="save picture"
                ),
                unsafe_allow_html=True,
            )

        with c1_3:
            enh_con = ImageEnhance.Contrast(image)
            contrast = st.slider(
                "",
                min_value=-100.0,
                max_value=100.0,
                value=1.0,
                step=0.5,
                key="contrast1",
            )
            image_contrasted = enh_con.enhance(contrast)
            st.image(
                image_contrasted, caption=f"Contrast: {contrast}", use_column_width=True
            )
            st.markdown(
                get_image_download_link(
                    img=image_contrasted, filename="test.png", text="save picture"
                ),
                unsafe_allow_html=True,
            )

        with c1_4:
            enh_sha = ImageEnhance.Sharpness(image)
            sharpness = st.slider(
                "",
                min_value=-100.0,
                max_value=100.0,
                value=1.0,
                step=0.5,
                key="sharpness1",
            )
            image_sharpened = enh_sha.enhance(sharpness)
            st.image(
                image_sharpened,
                caption=f" Sharpness: {sharpness}",
                use_column_width=True,
            )
            st.markdown(
                get_image_download_link(
                    img=image_sharpened, filename="test.png", text="save picture"
                ),
                unsafe_allow_html=True,
            )

    with st.beta_expander("Preprocessing - Gamma & Log"):  #
        c2_1, c2_2 = st.beta_columns((1, 1))

        with c2_1:
            gamma = st.slider(
                "Gamma",
                min_value=0.0,
                max_value=1.0,
                value=0.25,
                step=0.01,
                key="gamma1",
            )
            gain = st.slider(
                "Gain", min_value=0.0, max_value=1.0, value=1.0, step=0.01, key="gain1"
            )
            gamma_corrected = exposure.adjust_gamma(image=imgsk, gamma=gamma, gain=gain)
            st.image(
                gamma_corrected,
                caption=f"Gamma applied: {gamma} Gain applied: {gain}",
                use_column_width=True,
            )
            fig = get_exposure_histogram(
                bins=250, exposure_correction=gamma_corrected, imgsk=imgsk
            )
            st.pyplot(fig)
            result = Image.fromarray(
                img_as_ubyte(gamma_corrected)
            )  # format for saving file via download image function
            # change image download to be to mongo
            st.markdown(
                get_image_download_link(
                    img=result, filename="test.png", text="save picture"
                ),
                unsafe_allow_html=True,
            )

        with c2_2:
            gain = st.slider(
                "Gain", min_value=0.0, max_value=1.0, value=0.25, step=0.01, key="log1"
            )
            neglog = st.checkbox("Use negative log", key="neglog1")
            if neglog:
                inv = True
            else:
                inv = False

            st.write("   ")

            log_corrected = logarithmic_corrected = exposure.adjust_log(
                image=imgsk, gain=gain, inv=inv
            )
            st.image(
                log_corrected, caption=f"Gain applied: {gain}", use_column_width=True
            )
            fig = get_exposure_histogram(
                bins=250, exposure_correction=log_corrected, imgsk=imgsk
            )
            st.pyplot(fig)
            result = Image.fromarray(
                img_as_ubyte(log_corrected)
            )  # format for saving file via download image function
            # change image download to be to mongo
            st.markdown(
                get_image_download_link(
                    img=result, filename="test.png", text="save picture"
                ),
                unsafe_allow_html=True,
            )

    edges_plot = plot_image_edges(cv_img)
    st.title("Edge Masks")
    with st.beta_expander("Edges"):
        st.pyplot(edges_plot)

    st.title("View Blockage Risk")

    # change this to run model on selected images saved to database
    mypath = "data/sc_cropped"
    croppedfiles = [f"{mypath}/{f}" for f in listdir(mypath) if isfile(join(mypath, f))]
    croppedfiles
    image_clear = cv2.imread(croppedfiles[2], 0)
    image_blocked = cv2.imread(croppedfiles[3], 0)

    dict_blocked, dict_clear = get_edge_image_dicts(
        image_clear=image_clear, image_blocked=image_blocked
    )

    with st.beta_expander("Model"):
        for k in (k for k in dict_clear.keys() if k not in ["chan_vese_output"]):
            st.pyplot(
                compare_images_view(
                    dict_blocked=dict_blocked, dict_unblocked=dict_clear, filter_=k
                )
            )

    with st.beta_expander("View Results"):
        st.write("Dataframe of results")
