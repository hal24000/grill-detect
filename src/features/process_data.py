import base64
import io
from bson.binary import Binary

import cv2
import numpy as np

from setup.database import db


def ingest_image(site_name, site_id, site_state, image_loc):
    img = cv2.imread(image_loc)
    retval, buffer = cv2.imencode(".png", img)
    png_as_text = base64.b64encode(buffer)

    query = {
        "site_name": site_name,
        "site_id": site_id,
        "site_state": site_state,
    }
    update = {"$set": {"image": png_as_text}}
    db["kk_cv_images"].update(query, update)


def get_data(site_id, site_state):
    query = {"site_id": site_id, "site_state": site_state}
    project = {"_id": 0}
    data = list(db["kk_cv_images"].find(query, project))[0]
    png_original = base64.b64decode(data["image"])
    png_as_np = np.frombuffer(png_original, dtype=np.uint8)
    image_buffer = cv2.imdecode(png_as_np, flags=1)
    _ = data.pop("image")
    return data, image_buffer


def ingest_new_data(site_id, new_col, new_val):

    query = {
        "site_id": site_id,
    }
    update = {"$set": {new_col: new_val}}
    db["kk_cv_images"].update(query, update, multi=True)
